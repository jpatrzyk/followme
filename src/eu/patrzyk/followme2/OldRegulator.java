package eu.patrzyk.followme2;

import android.util.Log;

public class OldRegulator implements Regulator {
	
	private int width;
	private int height;
	
	private MoveCommand prevMovement;
	private int			movementIteration = 0;
	private int			iterationsNum = 5;
	private int			currentMovement = 0;
	
	public OldRegulator(int width, int height) {
		this.width = width;
		this.height = height;		
		this.prevMovement = new MoveCommand(MoveCommand.COMMAND_STOP, 0, 0);
	}

	public MoveCommand calculateMovement(BlobGroup blobs) {
		
		int closeMass = width * height / 4;	// 25% of the view
		int center, centerB, left, right, velocity, delta, vel2, del2;
		double m1, m2, v1, v2, d1, d2, a;
		
		// Miabot powinien jecha� szybciej do dalekiego obiektu �eby go dogoni�
		// a wolniej do bliskiego, �eby si� nie zderzy�
		v1 = 100;	// predkosc dla masy m1
		v2 = 40;	
		m1 = 100;	// masa dalekiego obiektu
		m2 = 10000;	// masa bliskiego obiektu
		d1 = 5;		// roznica predkosci kol dla m1
		d2 = 30;
		
		//Firstly, negative tropism - runaway from close blue objects
		// blobB[i].getMass() has 5 biggest masses, where blobB[0].getMass() is the largest
		
		int massB = blobs.negativeBlobs[0].getMass();
		if(massB > closeMass/2) {
			
			movementIteration = 0;
			
			centerB = (blobs.negativeBlobs[0].getxMin() + blobs.negativeBlobs[0].getxMax())/2;
			if(centerB < width/2) {	// on the left => turn right
				return new MoveCommand(MoveCommand.COMMAND_MOVE, 15, -15);
			}
			else {	// on the right => turn left
				return new MoveCommand(MoveCommand.COMMAND_MOVE, -15, 15);
			}
		}
		
		
		// Secondly, positive tropism - go towards the biggest red object
		int mass = blobs.positiveBlob.getMass();
		if(mass < m1) { // no red objects to follow - start looking for
			if(movementIteration < iterationsNum) {
				movementIteration++;
				if(currentMovement == 0) {
					return new MoveCommand(MoveCommand.COMMAND_MOVE, 40, 20);
				}
				else {
					return new MoveCommand(MoveCommand.COMMAND_MOVE, 20, 40);
				}
			}
			else {
				movementIteration = 0;
				if (currentMovement == 1) {
					prevMovement = new MoveCommand(MoveCommand.COMMAND_MOVE, 40, 20);
					currentMovement = 0;
					return prevMovement;
				}
				else {
					prevMovement = new MoveCommand(MoveCommand.COMMAND_MOVE, 20, 40);
					currentMovement = 1;
					return prevMovement;
				}
			}
			
		}
		// wida� obiekt, wi�c nie trzeba szuka�
		// obiekt jest do�� daleko
		// dziwne wyliczenia wynikaj� z zastosowania funkcji hiperbolicznej
		// (pr�dko�� jest hiperboliczn� funkcj� odleg�o�ci - patrz prezentacja)
		else if(mass < m2) {
			movementIteration = 0;
			
			center = (blobs.positiveBlob.getxMin() + blobs.positiveBlob.getxMax())/2;
			
			Log.v("MyLog", mass + ",  c = " + center);
			
			
			//velocity = (int)( ((v1-v2)/(m1-m2))*(mass-m1)+v1 );
			delta = (int)( ((d1-d2)/(m1-m2))*(mass-m1)+d1 );
			
			a = (v1*m1 - v2*m2)/(m1-m2);
			velocity = (int)( a + (v1-a)*m1/mass );
			
			if(center < width/4) {	// on the left
				prevMovement = new MoveCommand(MoveCommand.COMMAND_MOVE, velocity, velocity + delta);
				return prevMovement;
			}
			else if(center < 3*width/4) {	// ahead
				// the closer object is, the slower miabot move towards it
				
				return new MoveCommand(MoveCommand.COMMAND_MOVE, velocity, velocity);
			}
			else {	// on the right
				prevMovement = new MoveCommand(MoveCommand.COMMAND_MOVE, velocity + delta, velocity);
				return prevMovement;
			}
		}
		// obiekt jest blisko, ale nie na tyle �eby si� zatrzyma� lub cofa�
		else if(mass < closeMass) {
			movementIteration = 0;
			
			center = (blobs.positiveBlob.getxMin() + blobs.positiveBlob.getxMax())/2;			
			
			if(center < width/4) {	// on the left
				prevMovement = new MoveCommand(MoveCommand.COMMAND_MOVE, 20, 40);
				return prevMovement;
			}
			else if(center < 3*width/4) {	// ahead
				// the closer object is, the slower miabot move towards it
				left = right = (int)(800/mass);
				return new MoveCommand(MoveCommand.COMMAND_MOVE, left, right);
			}
			else {	// on the right
				prevMovement = new MoveCommand(MoveCommand.COMMAND_MOVE, 40, 20);
				return prevMovement;
			}
		}
		else {	// red object is close to robot - STOP or move BACK
			movementIteration = 0;
			
			Log.v("MyLog", mass + "");
			
			if(mass < 3*closeMass/2) {				
				return new MoveCommand(MoveCommand.COMMAND_STOP, 0, 0);
			}	// mass is to close - move back
			else {
				return new MoveCommand(MoveCommand.COMMAND_MOVE, -40, -40);
			}			
		}
	}

}
