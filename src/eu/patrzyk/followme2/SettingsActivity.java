package eu.patrzyk.followme2;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.PreferenceActivity;
import android.view.Menu;



public class SettingsActivity extends PreferenceActivity {

	private SharedPreferences preferences;
	
	EditTextPreference edit_neg_sp;
	EditTextPreference edit_neg_kp;
	EditTextPreference edit_neg_ki;
	EditTextPreference edit_neg_kd;
	
	EditTextPreference edit_pos_sp;
	EditTextPreference edit_pos_kp;
	EditTextPreference edit_pos_ki;
	EditTextPreference edit_pos_kd;
	
	EditTextPreference edit_dist_sp;
	EditTextPreference edit_dist_kp;
	EditTextPreference edit_dist_ki;
	EditTextPreference edit_dist_kd;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.activity_settings);
		
		preferences = getSharedPreferences(MainActivity.PREFS_NAME, Activity.MODE_PRIVATE);
		
		edit_neg_sp = (EditTextPreference)findPreference("edit_neg_sp");
		edit_neg_kp = (EditTextPreference)findPreference("edit_neg_kp");
		edit_neg_ki = (EditTextPreference)findPreference("edit_neg_ki");
		edit_neg_kd = (EditTextPreference)findPreference("edit_neg_kd");
		
		edit_pos_sp = (EditTextPreference)findPreference("edit_pos_sp");
		edit_pos_kp = (EditTextPreference)findPreference("edit_pos_kp");
		edit_pos_ki = (EditTextPreference)findPreference("edit_pos_ki");
		edit_pos_kd = (EditTextPreference)findPreference("edit_pos_kd");
		
		edit_dist_sp = (EditTextPreference)findPreference("edit_dist_sp");
		edit_dist_kp = (EditTextPreference)findPreference("edit_dist_kp");
		edit_dist_ki = (EditTextPreference)findPreference("edit_dist_ki");
		edit_dist_kd = (EditTextPreference)findPreference("edit_dist_kd");
		
		
		
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		getPreferences();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.settings, menu);
				
		return true;
	}
	
	@Override
	protected void onPause() {
        //Zapisz preferencje
        setPreferences();
        super.onStop();
    }

	private void getPreferences() {
		edit_neg_sp.setText(preferences.getString(MainActivity.negSp, "" + 0.0));
		edit_neg_kp.setText(preferences.getString(MainActivity.negKp, "" + 0.0));
		edit_neg_ki.setText(preferences.getString(MainActivity.negKi, "" + 0.0));
		edit_neg_kd.setText(preferences.getString(MainActivity.negKd, "" + 0.0));
		
		edit_pos_sp.setText(preferences.getString(MainActivity.posSp, "" + 0.0));
		edit_pos_kp.setText(preferences.getString(MainActivity.posKp, "" + 0.0));
		edit_pos_ki.setText(preferences.getString(MainActivity.posKi, "" + 0.0));
		edit_pos_kd.setText(preferences.getString(MainActivity.posKd, "" + 0.0));
		
		edit_dist_sp.setText(preferences.getString(MainActivity.distSp, "" + 0.0));
		edit_dist_kp.setText(preferences.getString(MainActivity.distKp, "" + 0.0));
		edit_dist_ki.setText(preferences.getString(MainActivity.distKi, "" + 0.0));
		edit_dist_kd.setText(preferences.getString(MainActivity.distKd, "" + 0.0));
	}
	
	private void setPreferences() {
		SharedPreferences.Editor editor = preferences.edit();
		
        editor.putString(MainActivity.negSp, edit_neg_sp.getText());
        editor.putString(MainActivity.negKp, edit_neg_kp.getText());
        editor.putString(MainActivity.negKi, edit_neg_ki.getText());
        editor.putString(MainActivity.negKd, edit_neg_kd.getText());
        
        editor.putString(MainActivity.posSp, edit_pos_sp.getText());
        editor.putString(MainActivity.posKp, edit_pos_kp.getText());
        editor.putString(MainActivity.posKi, edit_pos_ki.getText());
        editor.putString(MainActivity.posKd, edit_pos_kd.getText());
        
        editor.putString(MainActivity.distSp, edit_dist_sp.getText());
        editor.putString(MainActivity.distKp, edit_dist_kp.getText());
        editor.putString(MainActivity.distKi, edit_dist_ki.getText());
        editor.putString(MainActivity.distKd, edit_dist_kd.getText());
        
        editor.commit();
	}

}
