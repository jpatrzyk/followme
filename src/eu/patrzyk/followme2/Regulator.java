package eu.patrzyk.followme2;

public interface Regulator {

	MoveCommand calculateMovement(BlobGroup blobs);
}
