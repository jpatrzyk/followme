package eu.patrzyk.followme2;

import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.hardware.Camera;
import android.hardware.Camera.PreviewCallback;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

/** 
 * G��wna aplikacja.
 * Najwa�niejsza cz�� to metoda onPreviewFrame(byte[] data, Camera camera).
 * Jest ona wywo�ywana dla ka�dej klatki obrazu.
 * Nasz telefon nie wyrabia� z ilo�ci� klatek, wi�c wprowadzili�my zmienn� boolean frameToProcess,
 * kt�ra jest prze��czana i w efekcie przetwarzamy tylko co drug� klatk� obrazu.
 * Na pocz�tku obraz jest przetwarzany w ImageProcessor.
 * Nast�pnie przez Bluetooth wysy�amy komend� ruchu - decyduje o niej te� ImageProcessor.
 * Na ko�cu rozpoznane obiekty s� wy�wietlane przez DrawOnTop (te, za kt�rymi pod��a na czerwono,
 * a te od kt�rych ucieka na niebiesko).
 *
 */
public class MainActivity extends Activity implements PreviewCallback {
	
	public static final String PREFS_NAME = "FollowMeFile";
	
	// Intent request codes
    private static final int REQUEST_CONNECT_DEVICE = 1;
    private static final int REQUEST_ENABLE_BT = 2;
    
    // Message types sent from the BluetoothCommandService Handler
    public static final int MESSAGE_DEVICE_NAME = 3;
    public static final int MESSAGE_TOAST = 4;
    public static final int MESSAGE_CONNECT_FAILED = 5;
    public static final int MESSAGE_CONNECT_LOST = 6;
    
    // Key names received from the BluetoothCommandService Handler
    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast";
    
	private final int imageWidth = 320;
	private final int imageHeight = 240;
	
	public static final String negSp = "NegSP2";
	public static final String negKp = "NegKp2";
	public static final String negKi = "NegKi2";
	public static final String negKd = "NegKd2";
	
	public static final String posSp = "PosSP2";
	public static final String posKp = "PosKp2";
	public static final String posKi = "PosKi2";
	public static final String posKd = "PosKd2";
	
	public static final String distSp = "DistSP2";
	public static final String distKp = "DistKp2";
	public static final String distKi = "DistKi2";
	public static final String distKd = "DistKd2";
	
	
	// Name of the connected device
    private String mConnectedDeviceName = null;
    // Local Bluetooth adapter
    private BluetoothAdapter mBluetoothAdapter = null;
    // Member object for Bluetooth Command Service
    private BluetoothCommandService mCommandService = null;
    
    // Preview from Camera
    private Preview mPreview;
    
    // Drawings on the preview
	private DrawOnTop mDrawOnTop;
	
	// Processing of the frames from Camera - finding color blobs
	private ImageProcessor mProcessor = new ImageProcessor(imageWidth, imageHeight);
	
	//Regulator - calculates the movement basing on blobs location
	private Regulator mRegulator;
	
	// The Handler that gets information back from the BluetoothCommandService
    private final Handler mHandler = new IncomingHandler(this);
    
    private boolean calibrationPositive;
    private boolean calibrationNegative;
    
	private MenuItem buttonPos;
	private MenuItem buttonNeg;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        Log.v("MyLog", "onCreate");
        
      //Nie wy��czaj ekranu, gdy program jest na pierwszym planie
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        // Ukryj tytu� okna
     	getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
     	requestWindowFeature(Window.FEATURE_NO_TITLE);

		// Create our Preview view and set it as the content of our activity.
		// Create our DrawOnTop view.

		mDrawOnTop = new DrawOnTop(this, imageWidth, imageHeight);
		mPreview = new Preview(this, imageWidth, imageHeight);
		
		Log.v("MyLog", "MainActivity onCreate, before setContentView(mPreview)");

		setContentView(mPreview);
		addContentView(mDrawOnTop, 
				new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
        
        // Get local Bluetooth adapter
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        
        Log.v("MyLog", "onCreate, got Bluetooth adapter");

        // If the adapter is null, then Bluetooth is not supported
        if (mBluetoothAdapter == null) {
            Toast.makeText(this, R.string.no_bluetooth, Toast.LENGTH_LONG).show();
            finish();
            return;
        }
        
        Log.v("MyLog", "onCreate, bluetooth adapter != null");
    }

	@Override
	protected void onStart() {
		super.onStart();
		
		Log.v("MyLog", "onStart");
		
		// If BT is not on, request that it be enabled.
        // setupCommand() will then be called during onActivityResult
		if (!mBluetoothAdapter.isEnabled()) {
			Log.v("MyLog", "onStart, not enabled");
			
			Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
			startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
		}
		// otherwise set up the command service
		else {
			Log.v("MyLog", "onStart, enabled");
			
			if (mCommandService==null)
				setupCommand();
		}
		
		double[] parametersNeg = new double[4];
		double[] parametersPos = new double[4];
		double[] parametersDist = new double[4];
		
		SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
	    	   	parametersNeg[0] = Double.valueOf(settings.getString(negSp, "" + 50.0));
	    	   	parametersNeg[1] = Double.valueOf(settings.getString(negKp, "" +  0.001));
	    	   	parametersNeg[2] = Double.valueOf(settings.getString(negKi, "" + 0.0001));
	    	   	parametersNeg[3] = Double.valueOf(settings.getString(negKd, "" + 0.0001));
	    	   	
	    	   	parametersPos[0] = Double.valueOf(settings.getString(posSp, "" + imageWidth/2));
	    	   	parametersPos[1] = Double.valueOf(settings.getString(posKp, "" + 0.15));
	    	   	parametersPos[2] = Double.valueOf(settings.getString(posKi, "" + 0.0001));
	    	   	parametersPos[3] = Double.valueOf(settings.getString(posKd, "" + 0.0001));
	    	   	
	    	   	parametersDist[0] = Double.valueOf(settings.getString(distSp, "" +  imageWidth*imageHeight*0.2));
	    	   	parametersDist[1] = Double.valueOf(settings.getString(distKp, "" + 0.005));
	    	   	parametersDist[2] = Double.valueOf(settings.getString(distKi, "" + 0.00001));
	    	   	parametersDist[3] = Double.valueOf(settings.getString(distKd, "" + 0.00001));

	       mRegulator = new PIDRegulator(imageWidth, imageHeight, parametersNeg, parametersPos, parametersDist);
	}
	
	protected void onPause() {
		super.onPause();
		
		// We need an Editor object to make preference changes.
	      // All objects are from android.context.Context
	      SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
	      SharedPreferences.Editor editor = settings.edit();
	      
	      double[] parametersNeg = ((PIDRegulator)mRegulator).getPIDNegParameters();
	      double[] parametersPos = ((PIDRegulator)mRegulator).getPIDPosParameters();
	      double[] parametersDist = ((PIDRegulator)mRegulator).getPIDDistParameters();
	      
	      editor.putString(negSp, "" + parametersNeg[0]);
	      editor.putString(negKp, "" + parametersNeg[1]);
	      editor.putString(negKi, "" + parametersNeg[2]);
	      editor.putString(negKd, "" + parametersNeg[3]);
  	   	
	      editor.putString(posSp, "" + parametersPos[0]);
	      editor.putString(posKp, "" + parametersPos[1]);
	      editor.putString(posKi, "" + parametersPos[2]);
	      editor.putString(posKd, "" + parametersPos[3]);
  	   	
	      editor.putString(distSp, "" + parametersDist[0]);
	      editor.putString(distKp, "" + parametersDist[1]);
	      editor.putString(distKi, "" + parametersDist[2]);
	      editor.putString(distKd, "" + parametersDist[3]);
	      
	      // Commit the edits!
	      editor.commit();
	}
	
	@Override
	protected void onRestart() {
		super.onRestart();
		Log.v("MyLog", "onRestart");

		mDrawOnTop = new DrawOnTop(this, imageWidth, imageHeight);
		mPreview = new Preview(this, imageWidth, imageHeight);

		setContentView(mPreview);
		addContentView(mDrawOnTop, 
				new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
	}
	
	
	@Override
	protected void onResume() {
		super.onResume();
		Log.v("MyLog", "onResume");
		
		// Performing this check in onResume() covers the case in which BT was
        // not enabled during onStart(), so we were paused to enable it...
        // onResume() will be called when ACTION_REQUEST_ENABLE activity returns.
		if (mCommandService != null) {
			if (mCommandService.getState() == BluetoothCommandService.STATE_NONE) {
				
				Log.v("MyLog", "onResume, state == NONE, before service.start()");
				mCommandService.start();
			}
		}
	}

	private void setupCommand() {
		// Initialize the BluetoothCommandService to perform bluetooth connections
        mCommandService = new BluetoothCommandService(this, mHandler);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		
		if (mCommandService != null)
			mCommandService.stop();
	}
	
	private void ensureDiscoverable() {
        if (mBluetoothAdapter.getScanMode() !=
            BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
            Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
            discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
            startActivity(discoverableIntent);
        }
    }
	
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
        case REQUEST_CONNECT_DEVICE:
            // When DeviceListActivity returns with a device to connect
            if (resultCode == Activity.RESULT_OK) {
                // Get the device MAC address
                String address = data.getExtras()
                                     .getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
                // Get the BluetoothDevice object
                BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
                // Attempt to connect to the device
                mCommandService.connect(device);
            }
            break;
        case REQUEST_ENABLE_BT:
            // When the request to enable Bluetooth returns
            if (resultCode == Activity.RESULT_OK) {
                // Bluetooth is now enabled, so set up a chat session
                setupCommand();
            } else {
                // User did not enable Bluetooth or an error occured
                Toast.makeText(this, R.string.bt_not_enabled_leaving, Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
		buttonPos = menu.getItem(0);
	    buttonNeg =  menu.getItem(1);
        return true;
	}
	
	@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case R.id.action_bluetooth:
            // Launch the DeviceListActivity to see devices and do scan
        	Intent serverIntent = new Intent(this, DeviceListActivity.class);
            startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
            return true;
        case R.id.action_calibration_pos:
        	if (calibrationPositive) {
        		buttonPos.setTitle(R.string.action_calibration_pos);
	        	buttonNeg.setEnabled(true);
	        	calibrationPositive = false;
        	} else {
	        	buttonPos.setTitle(R.string.action_stop);
	        	buttonNeg.setEnabled(false);
	        	mProcessor.resetPositive();
	        	calibrationPositive = true;
        	}
        	Toast.makeText(getApplicationContext(), R.string.action_calibration_pos,
                    Toast.LENGTH_SHORT).show();
            return true;
        case R.id.action_calibration_neg:
        	if (calibrationNegative) {
        		buttonNeg.setTitle(R.string.action_calibration_neg);
	        	buttonPos.setEnabled(true);
	        	calibrationNegative = false;
        	} else {
	        	buttonNeg.setTitle(R.string.action_stop);
	        	buttonPos.setEnabled(false);
	        	mProcessor.resetNegative();
	        	calibrationNegative = true;
        	}

        	Toast.makeText(getApplicationContext(), R.string.action_calibration_neg,
                    Toast.LENGTH_SHORT).show();
            return true;
        case R.id.action_settings:
        	Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
            return true;
        }
        return false;
    }
	
	/**
	 * Metoda jest wywo�ywana dla ka�dej klatki obrazu.
	 * Nasz telefon nie wyrabia� z ilo�ci� klatek, wi�c wprowadzili�my zmienn� boolean frameToProcess,
	 * kt�ra jest prze��czana i w efekcie przetwarzamy tylko co drug� klatk� obrazu.
	 * Na pocz�tku obraz jest przetwarzany w ImageProcessor.
	 * Nast�pnie przez Bluetooth wysy�amy komend� ruchu - decyduje o niej te� ImageProcessor.
	 * Na ko�cu rozpoznane obiekty s� wy�wietlane przez DrawOnTop (te, za kt�rymi pod��a na czerwono,
	 * a te od kt�rych ucieka na niebiesko).
	 */
	boolean frameToProcess = true;
	public void onPreviewFrame(byte[] data, Camera camera) {
		if (mDrawOnTop == null) {
			Log.v("MyLog", "(mDrawOnTop == null) || mFinished");
			return;
		}
		mProcessor.setYUVData(data);
		if (calibrationPositive) {
			mProcessor.calibratePositive();
			mDrawOnTop.setStringToDrawYUV1(mProcessor.getStringYUV1());
			mDrawOnTop.setStringToDrawYUV2(mProcessor.getStringCurr());
		} else if (calibrationNegative) {
			mProcessor.calibrateNegative();
			mDrawOnTop.setStringToDrawYUV1(mProcessor.getStringYUV2());
			mDrawOnTop.setStringToDrawYUV2(mProcessor.getStringCurr());
		} else {
			if(frameToProcess) {
				// Pass YUV data to draw-on-top companion
				
				BlobGroup blobs = mProcessor.decodeYUV420SP_LocateBlobs();
				
				MoveCommand movement = mRegulator.calculateMovement(blobs);
				
				sendCommand(movement);
				
				mDrawOnTop.setStringToDraw(movement.toString());
				mDrawOnTop.setStringToDrawYUV1("");
				mDrawOnTop.setStringToDrawYUV2("");
				//Log.v("MyLog", "invalidate");
				//double[] d = ((PIDRegulator) mRegulator).getPIDNegParameters();
				//String s =  d[0] + " " + d[1] + " " + d[2] +" " + d[3];
				//mDrawOnTop.setStringToDraw(s);
				
				frameToProcess = false;
			}
			else {
				frameToProcess = true;
			}		
		}
		mDrawOnTop.setPixelsRGB(mProcessor.getRGBData());
		
		mDrawOnTop.invalidate();
	}

	
	/**
	 * ImageProcessor przesy�a prost� klas� opisuj�c� lokalizacje obiektow pozytywnych
	 * i negatywnych.
	 * Regulator na podstawie tych lokalizacji oblicza, jaki ruch Miabot ma wykona�.
	 * Metoda sendCommand wysy�a Miabotowi konkretn� komend�,
	 * w trybie tekstowym - wed�ug instrukcji Miabota.
	 */
	public void sendCommand(MoveCommand moveCommand) {		
		String toSend = moveCommand.toString();
		try {
			if(mCommandService != null) {
				mCommandService.write(toSend.getBytes("ASCII"));
			}			
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Umozliwia odbieranie wiadomosci od modulu Bluetooth.
	 * Odbierane wiadomosci:
	 *  - nazwa urzadzenia podlaczonego przez Bluetooth
	 *  - wiadomosc o zerwaniu polaczenia Bluetooth
	 *  - wiadomosc o niemoznosci polaczenia sie z wybranym urzadzeniem
	 */
	public void handleMessage(Message msg) {
		switch (msg.what) {
        case MESSAGE_DEVICE_NAME:
            // save the connected device's name
            mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
            Toast.makeText(getApplicationContext(), R.string.connected_to
                           + mConnectedDeviceName, Toast.LENGTH_SHORT).show();
            break;
        case MESSAGE_CONNECT_FAILED:
            Toast.makeText(getApplicationContext(), R.string.unable_to_connect,
                           Toast.LENGTH_SHORT).show();
            break;
        case MESSAGE_CONNECT_LOST:
            Toast.makeText(getApplicationContext(), R.string.connection_lost,
                           Toast.LENGTH_SHORT).show();
            break;
        }
	}
	
	
	static class IncomingHandler extends Handler {
	    private final WeakReference<MainActivity> mActivity; 

	    IncomingHandler(MainActivity activity) {
	        mActivity = new WeakReference<MainActivity>(activity);
	    }
	    
	    @Override
	    public void handleMessage(Message msg)
	    {
	    	MainActivity activity = mActivity.get();
	         if (activity != null) {
	        	 activity.handleMessage(msg);
	         }
	    }
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
	  // ignore orientation/keyboard change
	  super.onConfigurationChanged(newConfig);
	}
}