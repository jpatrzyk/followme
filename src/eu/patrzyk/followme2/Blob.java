package eu.patrzyk.followme2;

public class Blob {
	
	private int xMin;
	private int xMax;
	private int yMin;
	private int yMax;
	private int mass;
	
	
	public Blob(int xMin, int xMax, int yMin, int yMax, int mass) {
		super();
		this.xMin = xMin;
		this.xMax = xMax;
		this.yMin = yMin;
		this.yMax = yMax;
		this.mass = mass;
	}


	public int getxMin() {
		return xMin;
	}
	public int getxMax() {
		return xMax;
	}
	public int getyMin() {
		return yMin;
	}
	public int getyMax() {
		return yMax;
	}
	public int getMass() {
		return mass;
	}

}
