package eu.patrzyk.followme2;

public class BlobGroup {

	static public final int NEGATIVE_BLOBS_NUM = 5;
	
	public Blob[]		negativeBlobs = new Blob[NEGATIVE_BLOBS_NUM];
	public Blob			positiveBlob;

}
