package eu.patrzyk.followme2;

import java.io.IOException;
import java.util.List;

import android.hardware.Camera;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class Preview extends SurfaceView implements SurfaceHolder.Callback {

	SurfaceHolder mHolder;
	Camera mCamera;

	MainActivity mainActivity;

	int imageWidth;
	int imageHeight;

	@SuppressWarnings("deprecation")
	Preview(MainActivity context, int width, int height) {
		super(context);

		Log.v("MyLog", "Preview constructor");

		mainActivity = context;

		imageWidth = width;
		imageHeight = height;

		// Install a SurfaceHolder.Callback so we get notified when the
		// underlying surface is created and destroyed.
		mHolder = getHolder();
		mHolder.addCallback(this);
		mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
	}

	public void surfaceCreated(SurfaceHolder holder) {

		Log.v("MyLog", "surfaceCreated");

		mCamera = Camera.open();

		try {
			// mCamera.setPreviewDisplay(holder);
			mCamera.setPreviewDisplay(null);

			Log.v("MyLog", "setPreviewDisplay");

			// Preview callback used whenever new viewfinder frame is available
			mCamera.setPreviewCallback(mainActivity);

			Log.v("MyLog", "setPreviewCallback");
		} catch (IOException exception) {
			mCamera.release();
			mCamera = null;
		}
	}

	public void surfaceDestroyed(SurfaceHolder holder) {
		// Surface will be destroyed when we return, so stop the preview.
		// Because the CameraDevice object is not a shared resource, it's very
		// important to release it when the activity is paused.

		Log.v("MyLog", "surfaceDestroyed");
		
		Camera.Parameters parameters = mCamera.getParameters();
		//parameters.setFlashMode(parameters.FLASH_MODE_AUTO);
		mCamera.setParameters(parameters);
		
		mCamera.setPreviewCallback(null);
		mCamera.stopPreview();
		mCamera.release();
		mCamera = null;

	}

	public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
		// Now that the size is known, set up the camera parameters and begin
		// the preview.

		Log.v("MyLog", "surfaceChanged");

		Camera.Parameters parameters = mCamera.getParameters();

		List<int[]> fpslist = parameters.getSupportedPreviewFpsRange();
		Log.v("camera", "fpslist size = " + fpslist.size());
		for (int i = 0; i < fpslist.size(); i++) {
			Log.v("camera", i + " fps min = "
					+ fpslist.get(i)[Camera.Parameters.PREVIEW_FPS_MIN_INDEX]);
			Log.v("camera", i + " fps max = "
					+ fpslist.get(i)[Camera.Parameters.PREVIEW_FPS_MAX_INDEX]);
		}

		parameters.setPreviewSize(imageWidth, imageHeight);

		// parameters.setSceneMode(Camera.Parameters.SCENE_MODE_NIGHT);
		// parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
		//parameters.setFlashMode(parameters.FLASH_MODE_TORCH);
		mCamera.setParameters(parameters);
		mCamera.startPreview();

	}

}
