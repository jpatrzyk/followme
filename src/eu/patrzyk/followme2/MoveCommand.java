package eu.patrzyk.followme2;

import android.util.Log;

public class MoveCommand {

	public static final int COMMAND_MOVE = 1;
	public static final int COMMAND_STOP = 2;
	
	private int type;
	private int leftV;
	private int rightV;
	
	public MoveCommand(int what, int left, int right) {
		type = what;
				
		if(right > 200) {
			right = 200;
		}
		else if(right < -200) {
			right = -200;
		}
		rightV = right;
		
		if(left > 200) {
			left = 200;
		}
		else if(left < -200) {
			left = -200;
		}
		leftV = left;
		
		Log.v("MyLog", "move: " + left + " " + right);
	}
	
	public int getType() {
		return type;
	}

	public int getLeftV() {
		return leftV;
	}

	public int getRightV() {
		return rightV;
	}
	
	@Override
	public String toString() {		
		String toSend;		
		if(type == MoveCommand.COMMAND_STOP) {
			toSend = "[s]";
		}
		else {
			toSend = new StringBuffer("[=").append(leftV).append(",")
					.append(rightV).append("]").toString();
		}
		return toSend;
	}

}
