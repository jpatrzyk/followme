package eu.patrzyk.followme2.calibration;

/**
 * Klasa odpowiedzialna za przetwarzanie klatki obrazu.
 * Obraz z kamery jest w formacie YUV420.
 * Rozpoznawane s� na nim piksele okre�lonego koloru (domy�lnie Magenta i Green).
 * Wyszukiwane s� du�e grupy pikseli w jednym kolorze (BLOBy).
 * Metoda calculateMovement na podstawie lokalizacji BLOB�w decyduje o ruchu Miabota.
 * Tablica mRGBData zawiera piksele zakodowane w RGB do wy�wietlenia na ekranie:
 * obiekty, za kt�rymi Miabot pod��a s� malowane na czerwono, a te od kt�rych ucieka na niebiesko.
 */
public class CalibrationImageProcessor {

	private byte[] 		mYUVData;
	private int[] 		mRGBData;
	private int 		width;
	private int 		height;

	private int[] 		labelBuffer;
	private int[] 		labelTable;
	private int[] 		xMinTable;
	private int[] 		xMaxTable;
	private int[] 		yMinTable;
	private int[] 		yMaxTable;
	private int[] 		massTable;

	private int[] 		labelBufferB;
	private int[] 		labelTableB;
	private int[] 		xMinTableB;
	private int[] 		xMaxTableB;
	private int[] 		yMinTableB;
	private int[] 		yMaxTableB;
	private int[] 		massTableB;
	private boolean mode = false;
	
	private int prmin = 50;
	private int prmax = 100;
	private int pgmin = 50;
	private int pgmax = 100;
	private int pbmin = 50;
	private int pbmax = 100;
	
	private int nrmin = 150;
	private int nrmax = 200;
	private int ngmin = 150;
	private int ngmax = 200;
	private int nbmin = 150;
	private int nbmax = 200;
	
	private int curR = 150;
	private int curB = 200;
	private int curG = 150;

	private int centr = 0;
	private int centg = 0;
	private int centb = 0;
	
	private boolean first;
	int width1 = width / 2 - 10;
	int width2 = width /2 + 10;
	int height1 = height / 2 - 10;
	int height2 = height /2 + 10;
	private boolean modeNeg;
	
	public CalibrationImageProcessor(int imageWidth, int imageHeight) {
		width = imageWidth;
		height = imageHeight;
		width1 = width / 2 - 10;
		width2 = width /2 + 10;
		height1 = height / 2 - 10;
		height2 = height /2 + 10;

		// YUV NY21 format
		int dataLenght = (imageWidth * imageHeight * 3) / 2;

		mRGBData = new int[imageWidth * imageHeight]; 
		mYUVData = new byte[dataLenght]; 

		// The maximum number of blobs is given by an image filled with equally spaced single pixel
		// blobs. For images with less blobs, memory will be wasted, but this approach is simpler and
		// probably quicker than dynamically resizing arrays
		int tableSize = imageWidth * imageHeight / 4;

		labelBuffer = new int[imageWidth * imageHeight];

		labelTable = new int[tableSize];
		xMinTable = new int[tableSize];
		xMaxTable = new int[tableSize];
		yMinTable = new int[tableSize];
		yMaxTable = new int[tableSize];
		massTable = new int[tableSize];

		labelBufferB = new int[imageWidth * imageHeight];

		labelTableB = new int[tableSize];
		xMinTableB = new int[tableSize];
		xMaxTableB = new int[tableSize];
		yMinTableB = new int[tableSize];
		yMaxTableB = new int[tableSize];
		massTableB = new int[tableSize];

	}

	public void setYUVData(byte [] data) {
		System.arraycopy(data, 0, mYUVData, 0, data.length);
	}

	public int [] getRGBData() {
		return mRGBData;
	}

	public void decodeYUV420SP_LocateBlobs() {

		convertYUV420_NV21toRGB8888(mYUVData);

	}
	
	public int XYtoLin(int x, int y) {
		return width*y+x;
	}
	
	public void convertYUV420_NV21toRGB8888(byte [] data) {
	    int size = width*height;
	    int offset = size;
	    int[] pixels = mRGBData;
	    int u, v, y1, y2, y3, y4;

	    // i percorre os Y and the final pixels
	    // k percorre os pixles U e V
	    for(int i=0, k=0; i < size; i+=2, k+=2) {
	        y1 = data[i  ]&0xff;
	        y2 = data[i+1]&0xff;
	        y3 = data[width+i  ]&0xff;
	        y4 = data[width+i+1]&0xff;

	        u = data[offset+k  ]&0xff;
	        v = data[offset+k+1]&0xff;
	        u = u-128;
	        v = v-128;

	        pixels[i  ] = convertYUVtoRGB(y1, u, v);
	        pixels[i+1] = convertYUVtoRGB(y2, u, v);
	        pixels[width+i  ] = convertYUVtoRGB(y3, u, v);
	        pixels[width+i+1] = convertYUVtoRGB(y4, u, v);

	        if (i!=0 && (i+2)%width==0)
	            i+=width;
	    }

	    //return pixels;
	}

	private int convertYUVtoRGB(int y, int u, int v) {
	    int r,g,b;

	    r = y + (int)1.402f*v;
	    g = y - (int)(0.344f*u +0.714f*v);
	    b = y + (int)1.772f*u;
	    r = r>255? 255 : r<0 ? 0 : r;
	    g = g>255? 255 : g<0 ? 0 : g;
	    b = b>255? 255 : b<0 ? 0 : b;
	    return 0xff000000 | (b<<16) | (g<<8) | r;
	}

	public void switchMode() {
		if (this.mode) {
			this.mode = false;
		} else {
			this.mode  = true;
			this.first = true;
		}
		
	}
	
	public void switchModeNeg() {
		if (this.modeNeg) {
			this.modeNeg = false;
		} else {
			this.modeNeg  = true;
			this.first = true;
		}
		
	}
	
	public String getPos() {
		String s = "";
		s += "Pos: R(" + prmin + " , " + prmax + ") " + "G(" + pgmin + " , " + pgmax + ") " + "B(" + pbmin + " , " + pbmax + ") ";
		return s;
	}
	public String getNeg() {
		String s = "";
		s += "Neg: R(" + nrmin + " , " + nrmax + ") " + "G(" + ngmin + " , " + ngmax + ") " + "B(" + nbmin + " , " + nbmax + ") ";
		return s;
	}
	public String getAct() {
		String s = "";
		if (mode) {
			s += "Act: R( " + curR + " ) " + "G( " + curG + " ) " + "B( " + curB + " )";
		} else {
			s += "Cent: R( " + centr + " ) " + "G( " + centg + " ) " + "B( " + centb + " )";
		}
		return s;
	}

}

