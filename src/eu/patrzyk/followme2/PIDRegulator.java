package eu.patrzyk.followme2;

import android.util.Log;

public class PIDRegulator implements Regulator {
	private int width;
	private int height;
	private int optimalDist; // optymalny dystans od positive tropism
	
	private int maxSpeed = 50; // maksymalna pr�dko�� ko�a
	private int minSize = 50; // minimalny rozmiar rozpoznawanego obiektu
	
	private PIDController pidNegative;
	private PIDController pidPositive;
	private PIDController pidDistance;
	
	public PIDRegulator(int width, int height, double[] parametersNeg, double[] parametersPos, double[] parametersDist) {
		this.width = width;
		this.height = height;
		this.optimalDist = (int) (width * height * 0.20);
		
		pidNegative = new PIDController(parametersNeg[0], parametersNeg[1], parametersNeg[2], parametersNeg[3]);
		pidPositive = new PIDController(parametersPos[0], parametersPos[1], parametersPos[2], parametersPos[3]);
		pidDistance = new PIDController(parametersDist[0], parametersDist[1], parametersDist[2], parametersDist[3]);
		
//		pidNegative = new PIDController(50, 0.001, 0.0001, 0.0001);
//		pidPositive = new PIDController(width/2, 0.15, 0.0001, 0.0001);
//		pidDistance = new PIDController(optimalDist, 0.005, 0.00001, 0.00001);
		
	}
	
	public double[] getPIDNegParameters() {
		double[] d = new double[4];
		d[0] = pidNegative.getSetPoint();
		d[1] = pidNegative.getKp();
		d[2] = pidNegative.getKi();
		d[3] = pidNegative.getKd();
		
		return d;
	}
	
	public double[] getPIDPosParameters() {
		double[] d = new double[4];
		d[0] = pidPositive.getSetPoint();
		d[1] = pidPositive.getKp();
		d[2] = pidPositive.getKi();
		d[3] = pidPositive.getKd();
		
		return d;
	}
	
	public double[] getPIDDistParameters() {
		double[] d = new double[4];
		d[0] = pidDistance.getSetPoint();
		d[1] = pidDistance.getKp();
		d[2] = pidDistance.getKi();
		d[3] = pidDistance.getKd();
		
		return d;
	}
	
	private int normalizeSpeed(int speed) {
		int maxSize = 3*width*height;
		return speed*maxSpeed/maxSize;
	}

	public MoveCommand calculateMovement(BlobGroup blobs) {
		if (blobs.negativeBlobs[0].getMass() > minSize) { // negatywny tropizm
			int speed = (int)pidNegative.calculate(blobs.negativeBlobs[0].getMass());
			if ((blobs.negativeBlobs[0].getxMax() + blobs.negativeBlobs[0].getxMin())/2.0 < width/2.0) {
				return new MoveCommand(MoveCommand.COMMAND_MOVE, -speed, speed);
			} else {
				return new MoveCommand(MoveCommand.COMMAND_MOVE, speed, -speed);
			}
			
		} else if (blobs.positiveBlob.getMass() > minSize) { // pozytywny tropizm
			/*System.out.println("sp :  " + pidPositive.getSetPoint());
			System.out.println("   :  " + (blobs.positiveBlob.getxMax() + blobs.positiveBlob.getxMin())/2);
			System.out.println("Dir:  " + (int)pidPositive.calculate((blobs.positiveBlob.getxMax() + blobs.positiveBlob.getxMin())/2));
			System.out.println("Dist: " + (int)pidDistance.calculate(blobs.positiveBlob.getMass()));*/
			int dirSpeed = (int)pidPositive.calculate((blobs.positiveBlob.getxMax() + blobs.positiveBlob.getxMin())/2.0);
			int distSpeed = (int)pidDistance.calculate(blobs.positiveBlob.getMass());
			
			int leftSpeed = distSpeed - dirSpeed;
			int rightSpeed = distSpeed + dirSpeed;
			
			return new MoveCommand(MoveCommand.COMMAND_MOVE, leftSpeed, rightSpeed);
			
		} else {
			pidDistance.reset();
			pidNegative.reset();
			pidPositive.reset();
			return new MoveCommand(MoveCommand.COMMAND_STOP, 0, 0);
		}
	}

}
