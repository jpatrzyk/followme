package eu.patrzyk.followme2;

public class PIDController {
	private double setPoint;
	private double kp;
	private double ki;
	private double kd;
	private double previousError;
	private double integral;
	
	
	public PIDController(double setPoint, double kp, double ki, double kd) {
		this.setPoint = setPoint;
		this.kp = kp;
		this.ki = ki;
		this.kd = kd;
		this.previousError = 0;
		this.integral = 0;
	}
	
	public void reset() {
		this.previousError = 0;
		this.integral = 0;
	}
	
	public double calculate(double measuredValue) {
		double output = 0;
		
		double error = setPoint - measuredValue;
		integral = integral + error;
		double derivative = error - previousError;
		output = kp * error + ki * integral + kd * derivative;
		previousError = error;
		
		return output;
	}


	public double getKp() {
		return kp;
	}


	public void setKp(double kp) {
		this.kp = kp;
	}


	public double getKi() {
		return ki;
	}


	public void setKi(double ki) {
		this.ki = ki;
	}


	public double getKd() {
		return kd;
	}


	public void setKd(double kd) {
		this.kd = kd;
	}

	public double getSetPoint() {
		return setPoint;
	}

	public void setSetPoint(double setPoint) {
		this.setPoint = setPoint;
	}



	
	
	
}
