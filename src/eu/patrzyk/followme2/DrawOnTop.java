package eu.patrzyk.followme2;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;
import android.view.View;

public class DrawOnTop extends View {
	private Bitmap mBitmap;
	
	private Paint mPaintBlack;
	private Paint mPaintWhite;

	private int mImageWidth;
	private int mImageHeight;
	
	private String stringToDraw;
	
	private Rect srcRect;
	private Rect dstRect;
	
	private String stringToDrawYUV1;
	private String stringToDrawYUV2;

	public DrawOnTop(Context context, int width, int height) {
		super(context);

		Log.v("MyLog", "DrawOnTop constructor");
		
		mImageWidth = width;
		mImageHeight = height;
		mBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);

		mPaintBlack = new Paint();
		mPaintBlack.setStyle(Paint.Style.FILL);
		mPaintBlack.setColor(Color.BLACK);
		mPaintBlack.setTextSize(25);

		mPaintWhite = new Paint();
		mPaintWhite.setStyle(Paint.Style.FILL);
		mPaintWhite.setColor(Color.WHITE);
		mPaintWhite.setTextSize(15);
	
		srcRect = new Rect(0, 0, mImageWidth, mImageHeight);
		dstRect = new Rect(0, 0, mImageWidth, mImageHeight);
		stringToDraw = "";
		stringToDrawYUV1 = "";
		stringToDrawYUV2 = "";
		
		Log.v("MyLog", "DrawOnTop End of constructor");
	}
	
	public void setPixelsRGB(int [] mRGBData) {
		if (mBitmap != null)
		{
			mBitmap.setPixels(mRGBData, 0, mImageWidth, 0, 0, mImageWidth, mImageHeight);
		}		
	}
	
	public void setStringToDraw(String s) {
		this.stringToDraw = s;
	}


	@Override
	protected void onDraw(Canvas canvas) {
		if (mBitmap != null)
		{
			int canvasWidth = canvas.getWidth();
			int canvasHeight = canvas.getHeight();
			
			// Draw bitmap		
			dstRect.set(0, 0, canvasWidth, canvasHeight);
			canvas.drawBitmap(mBitmap, srcRect, dstRect, null);
			
			// Draw text
			canvas.drawText(stringToDraw, 10, canvasHeight-10, mPaintWhite);
			canvas.drawText(stringToDrawYUV1, 0, 25, mPaintWhite);
			canvas.drawText(stringToDrawYUV2, 0, 50, mPaintWhite);

		} // end if statement
		canvas.drawColor(0x00AAAAAA);
		super.onDraw(canvas);

	} // end onDraw method
	public void setStringToDrawYUV1(String s) {
		this.stringToDrawYUV1 = s;
		
	}

	public void setStringToDrawYUV2(String s) {
		this.stringToDrawYUV2 = s;
		
	}

} 