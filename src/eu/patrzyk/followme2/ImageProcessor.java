package eu.patrzyk.followme2;


/**
 * Klasa odpowiedzialna za przetwarzanie klatki obrazu.
 * Obraz z kamery jest w formacie YUV420.
 * Rozpoznawane s� na nim piksele okre�lonego koloru (domy�lnie Magenta i Green).
 * Wyszukiwane s� du�e grupy pikseli w jednym kolorze (BLOBy).
 * Metoda calculateMovement na podstawie lokalizacji BLOB�w decyduje o ruchu Miabota.
 * Tablica mRGBData zawiera piksele zakodowane w RGB do wy�wietlenia na ekranie:
 * obiekty, za kt�rymi Miabot pod��a s� malowane na czerwono, a te od kt�rych ucieka na niebiesko.
 */
public class ImageProcessor {

	private byte[] 		mYUVData;
	private int[] 		mRGBData;
	private int 		width;
	private int 		height;

	private int[] 		labelBuffer;
	private int[] 		labelTable;
	private int[] 		xMinTable;
	private int[] 		xMaxTable;
	private int[] 		yMinTable;
	private int[] 		yMaxTable;
	private int[] 		massTable;

	private int[] 		labelBufferB;
	private int[] 		labelTableB;
	private int[] 		xMinTableB;
	private int[] 		xMaxTableB;
	private int[] 		yMinTableB;
	private int[] 		yMaxTableB;
	private int[] 		massTableB;
	private boolean mode = false;
	
	private int prmin = 50;
	private int prmax = 100;
	private int pgmin = 50;
	private int pgmax = 100;
	private int pbmin = 50;
	private int pbmax = 100;
	
	private int nrmin = 150;
	private int nrmax = 200;
	private int ngmin = 150;
	private int ngmax = 200;
	private int nbmin = 150;
	private int nbmax = 200;
	
	private int curR = 150;
	private int curB = 200;
	private int curG = 150;

	private int centr = 0;
	private int centg = 0;
	private int centb = 0;
	
	private boolean firstNegative;
	private boolean firstPositive;
	int width1 = width / 2 - 10;
	int width2 = width /2 + 10;
	int height1 = height / 2 - 10;
	int height2 = height /2 + 10;
	private boolean modeNeg;
	
	private int mYUVDecoded[][];
	
	public ImageProcessor(int imageWidth, int imageHeight) {
		width = imageWidth;
		height = imageHeight;
		width1 = width / 2 - 10;
		width2 = width /2 + 10;
		height1 = height / 2 - 10;
		height2 = height /2 + 10;

		// YUV NY21 format
		int dataLenght = (imageWidth * imageHeight * 3) / 2;

		mRGBData = new int[imageWidth * imageHeight]; 
		mYUVData = new byte[dataLenght]; 
		mYUVDecoded = new int[imageWidth*imageHeight][3];

		// The maximum number of blobs is given by an image filled with equally spaced single pixel
		// blobs. For images with less blobs, memory will be wasted, but this approach is simpler and
		// probably quicker than dynamically resizing arrays
		int tableSize = imageWidth * imageHeight / 4;

		labelBuffer = new int[imageWidth * imageHeight];

		labelTable = new int[tableSize];
		xMinTable = new int[tableSize];
		xMaxTable = new int[tableSize];
		yMinTable = new int[tableSize];
		yMaxTable = new int[tableSize];
		massTable = new int[tableSize];

		labelBufferB = new int[imageWidth * imageHeight];

		labelTableB = new int[tableSize];
		xMinTableB = new int[tableSize];
		xMaxTableB = new int[tableSize];
		yMinTableB = new int[tableSize];
		yMaxTableB = new int[tableSize];
		massTableB = new int[tableSize];

	}

	public void setYUVData(byte [] data) {
		System.arraycopy(data, 0, mYUVData, 0, data.length);
	}

	public int [] getRGBData() {
		return mRGBData;
	}

	public BlobGroup decodeYUV420SP_LocateBlobs() {
		
		decodeYUV();
		
		//mRGBData = convertYUV420_NV21toRGB8888(mYUVData);
		
		BlobGroup returnBlobs = new BlobGroup();

		final int frameSize = width * height;

		// This is the neighbouring pixel pattern. For position X, A, B, C & D are checked
		// A B C
		// D X
		int srcPtr = 0;
		int aPtr = -width - 1;
		int bPtr = -width;
		int cPtr = -width + 1;
		int dPtr = -1;

		int label = 1;
		int labelB = 1;
		
//		int x = width/2;
//		int y = height/2;
//		centr = mRGBData[XYtoLin(x, y)] & (0xff0000);
//		centr = centr >> 16;
//		centg = mRGBData[XYtoLin(x, y)] & (0xff00);
//		centg = centg >> 8;
//		centb = mRGBData[XYtoLin(x, y)] & (0xff);


		for (int j = 0, yp = 0; j < height; j++) {
			int uvp = frameSize + (j >> 1) * width;//, u = 0, v = 0;
			for (int i = 0; i < width; i++, yp++) {
//				int y = (0xff & ((int) mYUVData[yp])) - 16;
//				if (y < 0) y = 0;
//				if ((i & 1) == 0) {
//					v = (0xff & mYUVData[uvp++]) - 128;
//					u = (0xff & mYUVData[uvp++]) - 128;
//				}
				
				int y = mYUVDecoded[yp][0];
				int u = mYUVDecoded[yp][1];
				int v = mYUVDecoded[yp][2];

				labelBuffer[srcPtr] = 0;
				labelBufferB[srcPtr] = 0;


				if( (y > nrmin) && (y < nrmax) && (u > ngmin) && (u < ngmax) && (v > nbmin) && (v < nbmax)) { // zielony
					
					mRGBData[yp] = convertYUVtoRGB(y, u, v);
					
					// Find label for neighbours (0 if out of range)
					int aLabel = (i > 0 && j > 0)	? labelTableB[labelBufferB[aPtr]] : 0;
					int bLabel = (j > 0)			? labelTableB[labelBufferB[bPtr]] : 0;
					int cLabel = (i < width-1 && j > 0)	? labelTableB[labelBufferB[cPtr]] : 0;
					int dLabel = (i > 0)			? labelTableB[labelBufferB[dPtr]] : 0;

					// Look for label with least value
					int min = Integer.MAX_VALUE;
					if (aLabel != 0 && aLabel < min) min = aLabel;
					if (bLabel != 0 && bLabel < min) min = bLabel;
					if (cLabel != 0 && cLabel < min) min = cLabel;
					if (dLabel != 0 && dLabel < min) min = dLabel;

					// If no neighbours in foreground
					if (min == Integer.MAX_VALUE)
					{
						labelBufferB[srcPtr] = labelB;
						labelTableB[labelB] = labelB;

						// Initialise min/max x,y for label
						yMinTableB[labelB] = j;
						yMaxTableB[labelB] = j;
						xMinTableB[labelB] = i;
						xMaxTableB[labelB] = i;
						massTableB[labelB] = 1;

						labelB ++;
					}

					// Neighbour found
					else
					{
						// Label pixel with lowest label from neighbours
						labelBufferB[srcPtr] = min;

						// Update min/max x,y for label
						yMaxTableB[min] = j;
						massTableB[min]++;
						if (i < xMinTableB[min]) xMinTableB[min] = i;
						if (i > xMaxTableB[min]) xMaxTableB[min] = i;

						if (aLabel != 0) labelTableB[aLabel] = min;
						if (bLabel != 0) labelTableB[bLabel] = min;
						if (cLabel != 0) labelTableB[cLabel] = min;
						if (dLabel != 0) labelTableB[dLabel] = min;
					}

				}
				// Else check if pixel is red - positive tropism

				else if( (y > prmin) && (y < prmax) && (u > pgmin) && (u < pgmax) && (v > pbmin) && (v < pbmax)) {// rozowy
					
					mRGBData[yp] = convertYUVtoRGB(y, u, v);
					
					// Find label for neighbours (0 if out of range)
					int aLabel = (i > 0 && j > 0)	? labelTable[labelBuffer[aPtr]] : 0;
					int bLabel = (j > 0)			? labelTable[labelBuffer[bPtr]] : 0;
					int cLabel = (i < width-1 && j > 0)	? labelTable[labelBuffer[cPtr]] : 0;
					int dLabel = (i > 0)			? labelTable[labelBuffer[dPtr]] : 0;

					// Look for label with least value
					int min = Integer.MAX_VALUE;
					if (aLabel != 0 && aLabel < min) min = aLabel;
					if (bLabel != 0 && bLabel < min) min = bLabel;
					if (cLabel != 0 && cLabel < min) min = cLabel;
					if (dLabel != 0 && dLabel < min) min = dLabel;

					// If no neighbours in foreground
					if (min == Integer.MAX_VALUE)
					{
						labelBuffer[srcPtr] = label;
						labelTable[label] = label;

						// Initialise min/max x,y for label
						yMinTable[label] = j;
						yMaxTable[label] = j;
						xMinTable[label] = i;
						xMaxTable[label] = i;
						massTable[label] = 1;

						label ++;
					}

					// Neighbour found
					else
					{
						// Label pixel with lowest label from neighbours
						labelBuffer[srcPtr] = min;

						// Update min/max x,y for label
						yMaxTable[min] = j;
						massTable[min]++;
						if (i < xMinTable[min]) xMinTable[min] = i;
						if (i > xMaxTable[min]) xMaxTable[min] = i;

						if (aLabel != 0) labelTable[aLabel] = min;
						if (bLabel != 0) labelTable[bLabel] = min;
						if (cLabel != 0) labelTable[cLabel] = min;
						if (dLabel != 0) labelTable[dLabel] = min;
					}

				}

				else {
					mRGBData[yp] = 0x00000000;
				}


				srcPtr ++;
				aPtr ++;
				bPtr ++;
				cPtr ++;
				dPtr ++;

			}
		}

		// Find the largest red object
		int maxMassIndex = 0;
		int mass = 0;
		// Iterate through labels pushing min/max x,y values towards minimum label
		for (int i=label-1 ; i>0 ; i--)
		{
			if (labelTable[i] != i)
			{
				if (xMaxTable[i] > xMaxTable[labelTable[i]]) xMaxTable[labelTable[i]] = xMaxTable[i];
				if (xMinTable[i] < xMinTable[labelTable[i]]) xMinTable[labelTable[i]] = xMinTable[i];
				if (yMaxTable[i] > yMaxTable[labelTable[i]]) yMaxTable[labelTable[i]] = yMaxTable[i];
				if (yMinTable[i] < yMinTable[labelTable[i]]) yMinTable[labelTable[i]] = yMinTable[i];
				massTable[labelTable[i]] += massTable[i];

				int l = i;
				while (l != labelTable[l]) l = labelTable[l];
				labelTable[i] = l;
			}
			else
			{
				if(massTable[i] > mass) {
					maxMassIndex = i;
					mass = massTable[i];
				}
			}			
		}

		returnBlobs.positiveBlob = new Blob(xMinTable[maxMassIndex],
				xMaxTable[maxMassIndex],
				yMinTable[maxMassIndex],
				yMaxTable[maxMassIndex],
				massTable[maxMassIndex]);
		

		// Find largest blue objects
		// int arrays are always initialized with 0
		int [] maxMassIndexB = new int[BlobGroup.NEGATIVE_BLOBS_NUM];
		int [] massB = new int[BlobGroup.NEGATIVE_BLOBS_NUM];
		int blobsNumber = BlobGroup.NEGATIVE_BLOBS_NUM;
		// Iterate through labels pushing min/max x,y values towards minimum label
		for (int i=labelB-1 ; i>0 ; i--)
		{
			if (labelTableB[i] != i)
			{
				if (xMaxTableB[i] > xMaxTableB[labelTableB[i]]) xMaxTableB[labelTableB[i]] = xMaxTableB[i];
				if (xMinTableB[i] < xMinTableB[labelTableB[i]]) xMinTableB[labelTableB[i]] = xMinTableB[i];
				if (yMaxTableB[i] > yMaxTableB[labelTableB[i]]) yMaxTableB[labelTableB[i]] = yMaxTableB[i];
				if (yMinTableB[i] < yMinTableB[labelTableB[i]]) yMinTableB[labelTableB[i]] = yMinTableB[i];
				massTableB[labelTableB[i]] += massTableB[i];

				int l = i;
				while (l != labelTableB[l]) l = labelTableB[l];
				labelTableB[i] = l;
			}
			else
			{
				int j = 0, k = -1;
				while(j<blobsNumber && massB[j] > massTableB[i]) {
					j++;
				}
				if(j<blobsNumber) {
					while(k > j) {
						massB[k] = massB[k-1];
						maxMassIndexB[k] = maxMassIndexB[k-1];
						k--;
					}
					massB[j] = massTableB[i];
					maxMassIndexB[j] = i;
					
				}
			}			
		}

		for(int i=0; i<blobsNumber; i++) {
			returnBlobs.negativeBlobs[i] = new Blob(xMinTableB[maxMassIndexB[i]],
					xMaxTableB[maxMassIndexB[i]],
					yMinTableB[maxMassIndexB[i]],
					yMaxTableB[maxMassIndexB[i]],
					massTableB[maxMassIndexB[i]]);
		}
		
		
		
		return returnBlobs;
	}
	
	public int XYtoLin(int x, int y) {
		return width*y+x;
	}
	
	private void decodeYUV() {
		int size = width*height;
	    int offset = size;
	    int u, v, y1, y2, y3, y4;
	    
		for(int i=0, k=0; i < size; i+=2, k+=2) {
	        y1 = mYUVData[i  ]&0xff;
	        y2 = mYUVData[i+1]&0xff;
	        y3 = mYUVData[width+i  ]&0xff;
	        y4 = mYUVData[width+i+1]&0xff;

	        u = mYUVData[offset+k  ]&0xff;
	        v = mYUVData[offset+k+1]&0xff;
	        u = u-128;
	        v = v-128;

	        mYUVDecoded[i  ][0] = y1;
	        mYUVDecoded[i  ][1] = u;
	        mYUVDecoded[i  ][2] = v;
	        
	        mYUVDecoded[i+1][0] = y2;
	        mYUVDecoded[i+1][1] = u;
	        mYUVDecoded[i+1][2] = v;
	        
	        mYUVDecoded[width+i  ][0] = y3;
	        mYUVDecoded[width+i  ][1] = u;
	        mYUVDecoded[width+i  ][2] = v;
	        
	        mYUVDecoded[width+i+1][0] = y4;
	        mYUVDecoded[width+i+1][1] = u;
	        mYUVDecoded[width+i+1][2] = v;

	        if (i!=0 && (i+2)%width==0)
	            i+=width;
	    }
	}
	
	public int[] convertYUV420_NV21toRGB8888(byte [] data) {
	    int size = width*height;
	    int offset = size;
	    int[] pixels = new int[size];
	    int u, v, y1, y2, y3, y4;

	    // i percorre os Y and the final pixels
	    // k percorre os pixles U e V
	    for(int i=0, k=0; i < size; i+=2, k+=2) {
	        y1 = data[i  ]&0xff;
	        y2 = data[i+1]&0xff;
	        y3 = data[width+i  ]&0xff;
	        y4 = data[width+i+1]&0xff;

	        u = data[offset+k  ]&0xff;
	        v = data[offset+k+1]&0xff;
	        u = u-128;
	        v = v-128;

	        pixels[i  ] = convertYUVtoRGB(y1, u, v);
	        pixels[i+1] = convertYUVtoRGB(y2, u, v);
	        pixels[width+i  ] = convertYUVtoRGB(y3, u, v);
	        pixels[width+i+1] = convertYUVtoRGB(y4, u, v);

	        if (i!=0 && (i+2)%width==0)
	            i+=width;
	    }

	    return pixels;
	}

	private int convertYUVtoRGB(int y, int u, int v) {
	    int r,g,b;

	    r = y + (int)1.402f*v;
	    g = y - (int)(0.344f*u +0.714f*v);
	    b = y + (int)1.772f*u;
	    r = r>255? 255 : r<0 ? 0 : r;
	    g = g>255? 255 : g<0 ? 0 : g;
	    b = b>255? 255 : b<0 ? 0 : b;
	    return 0xff000000 | (b<<16) | (g<<8) | r;
	}
	

	public void calibratePositive() {
		mRGBData = convertYUV420_NV21toRGB8888(mYUVData);
		int frameSize = width * height;
//		int u=0, v=0, y=0;
//		for (int j = 0, yp = 0; j < height; j++) {
//			int uvp = frameSize + (j >> 1) * width;
//			for (int i = 0; i < width; i++, yp++) {
//				y = (0xff & ((int) mYUVData[yp])) - 16;
//				if (y < 0) y = 0;
//				if ((i & 1) == 0) {
//					v = (0xff & mYUVData[uvp++]) - 128;
//					u = (0xff & mYUVData[uvp++]) - 128;
//				}
//				if (j >= height/2 && i >= width/2) {
//					break;
//				}
//			}
//		}
		decodeYUV();
		
		int yp = XYtoLin(width/2, height/2);
		
		int y = mYUVDecoded[yp][0];
		int u = mYUVDecoded[yp][1];
		int v = mYUVDecoded[yp][2];
		
		curR = y;
		curG = u;
		curB = v;
		
		if (firstPositive) {
				prmin = y;
				prmax = y;
				pgmin = u;
				pgmax = u;
				pbmin = v;
				pbmax = v;
			
			firstPositive = false;
		}
		
			if (y < prmin) {
				prmin = y;
			} else if (y > prmax) {
				prmax = y;
			}
			
			if (u < pgmin) {
				pgmin = u;
			} else if (u > pgmax) {
				pgmax = u;
			}
			
			if (v < pbmin) {
				pbmin = v;
			} else if (v > pbmax) {
				pbmax = v;
			}
		
	}
	
	public void resetPositive(){
		firstPositive = true;
	}
	
	public void resetNegative(){
		firstNegative = true;
	}

	public void calibrateNegative() {
		mRGBData = convertYUV420_NV21toRGB8888(mYUVData);
		int frameSize = width * height;
//		int u=0, v=0, y=0;
//		for (int j = 0, yp = 0; j < height; j++) {
//			int uvp = frameSize + (j >> 1) * width;
//			for (int i = 0; i < width; i++, yp++) {
//				y = (0xff & ((int) mYUVData[yp])) - 16;
//				if (y < 0) y = 0;
//				if ((i & 1) == 0) {
//					v = (0xff & mYUVData[uvp++]) - 128;
//					u = (0xff & mYUVData[uvp++]) - 128;
//				}
//				if (j >= height/2 && i >= width/2) {
//					break;
//				}
//			}
//		}
		
		decodeYUV();
		
		int yp = XYtoLin(width/2, height/2);
		
		int y = mYUVDecoded[yp][0];
		int u = mYUVDecoded[yp][1];
		int v = mYUVDecoded[yp][2];
		
		curR = y;
		curG = u;
		curB = v;
		
		if (firstNegative) {
				nrmin = y;
				nrmax = y;
				ngmin = u;
				ngmax = u;
				nbmin = v;
				nbmax = v;
			
			firstNegative = false;
		}
		
			if (y < nrmin) {
				nrmin = y;
			} else if (y > nrmax) {
				nrmax = y;
			}
			
			if (u < ngmin) {
				ngmin = u;
			} else if (u > ngmax) {
				ngmax = u;
			}
			
			if (v < nbmin) {
				nbmin = v;
			} else if (v > nbmax) {
				nbmax = v;
			}
		
	}


	public String getStringYUV1() {
		String s = "";
		s += "Pos: Y(" + prmin + " , " + prmax + ") " + "U(" + pgmin + " , " + pgmax + ") " + "V(" + pbmin + " , " + pbmax + ") ";
		return s;
	}

	public String getStringYUV2() {
		String s = "";
		s += "Neg: Y(" + nrmin + " , " + nrmax + ") " + "U(" + ngmin + " , " + ngmax + ") " + "V(" + nbmin + " , " + nbmax + ") ";
		return s;
	}

	public String getStringCurr() {
		String s = "";
		s += "Act: Y( " + curR + " ) " + "U( " + curG + " ) " + "V( " + curB + " )";
		return s;
	}

}
